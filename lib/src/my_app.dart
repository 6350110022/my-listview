import 'package:flutter/material.dart';
import 'package:listview111/src/pages/example1.dart';
import 'package:listview111/src/pages/example2.dart';
import 'package:listview111/src/pages/example3.dart';
import 'package:listview111/src/pages/example4.dart';


class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.orange,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Example4(),
    );
  }
}
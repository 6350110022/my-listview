import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Example4 extends StatelessWidget {
  Example4({Key? key}) : super(key: key);

  final titles = [
    'Red Velvet Macarons',
    'Belgian Waffle',
    'Churos',
    'Butter Croissant',
    'Nutella Berry Crepes',
    'Berry and White chocolate Mille Feuille',
    'Almond Pancake with Raspberries',
    'Cranberry White Chocolate Tart',
    'Strawberry Eclair'
  ];

  final images = [
    AssetImage('asset/img/image1.jpg'),
    AssetImage('asset/img/image2.jpg'),
    AssetImage('asset/img/image3.jpg'),
    AssetImage('asset/img/image4.jpg'),
    AssetImage('asset/img/image5.jpg'),
    AssetImage('asset/img/image6.jpg'),
    AssetImage('asset/img/image7.jpg'),
    AssetImage('asset/img/image8.jpg'),
    AssetImage('asset/img/image9.jpg')
  ];

  final subtitle = [
    'Macarons',
    'Waffle',
    'Churos',
    'Croissant',
    'Crepes',
    'Mille Feuille',
    'Pancake',
    'Tart',
    'Eclair'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListView4'),
      ),
      body: ListView.builder(
        itemCount: titles.length,
        itemBuilder: (context, index) {
          return Column(
            children: [
              ListTile(
                leading: CircleAvatar(
                  backgroundImage: images[index],
                  radius: 30,
                ),
                title: Text(
                  '${titles[index]}',
                  style: TextStyle(fontSize: 18),
                ),
                subtitle: Text(
                  subtitle[index],
                  style: TextStyle(fontSize: 15),
                ),
                trailing: const Icon(
                  Icons.storefront,
                  size: 25,
                ),
                onTap: () {
                  Fluttertoast.showToast(
                    msg: '${titles[index]}',
                    toastLength: Toast.LENGTH_SHORT,
                  ); //print('${titles[index]}');
                },
              ),
              Divider(
                thickness: 6,
              ),
            ],
          );
        },
      ),
    );
  }
}
